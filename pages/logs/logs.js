//logs.js
const util = require('../../utils/util.js')

Page({
  data: {
    logs: []
  },
  onLoad: function() {
    this.setData({
      logs: (wx.getStorageSync('logs') || []).map(log => {
        return util.formatTime(new Date(log))
      })
    })
  },
  onShareAppMessage: function(o) {
    if (o.from == 'button') {
      console.log(o.targert)
    }
    return {
      title: '自定义转发标题',
      path: '/page/user?id=123'
    }
  },

  viewTap: function() {
    console.log('view tap')
  }

})