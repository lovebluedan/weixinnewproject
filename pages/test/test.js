// pages/test/test.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    message: '第一次使用这个方法',
    array: [1, 2, 3, 4, 5],
    zero: 0,
    a: 1,
    b: 2,
    obj1: {
      a: 1,
      b: 2
    },
    obj2: {
      c: 3,
      d: 4
    },
    objectArray: [{
        id: 5,
        unique: 'unique_5'
      },
      {
        id: 4,
        unique: 'unique_4'
      },
      {
        id: 3,
        unique: 'unique_3'
      },
      {
        id: 2,
        unique: 'unique_2'
      },
      {
        id: 1,
        unique: 'unique_1'
      },
      {
        id: 0,
        unique: 'unique_0'
      },
    ],
    numberArray: [1, 2, 3, 4],
    length: 6,
    item:{
      message:"hhehe",
      time:new Date()
    }

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options.query)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  arryData: function() {
    this.setData({
      array: [3, 4, 5, 6, 7],
      message: '你好这个是第几次了'
    })
  },
  objectCombine: function() {
    a: {
      s,
      s,
      s,
      d,
      d,
      d,
      f
    };
    b: {
      a,
      a,
      a,
      a,
      a,
      a
    }
  },
  switch: function(e) {
    const length = this.data.objectArray.length
    for (let i = 0; i < length; ++i) {
      const x = Math.floor(Math.random() * length)
      const y = Math.floor(Math.random() * length)
      const temp = this.data.objectArray[x]
      this.data.objectArray[x] = this.data.objectArray[y]
      this.data.objectArray[y] = temp
    }
    //修改好了数据 就要把数据setData到原来的数据中去了
    this.setData({
      objectArray: this.data.objectArray
    })
  },
  addToFront: function(e) {
    const length = this.data.objectArray.length
    this.data.objectArray = [{
      id: length,
      unique: 'unique_' + length
    }].concat(this.data.objectArray)
    //修改好了数据 就要把数据setData到原来的数据中去了
    this.setData({
      objectArray: this.data.objectArray
    })
  },
  addNumberToFront: function(e) {
    this.data.numberArray = [this.data.numberArray.length + 1].concat(this.data.numberArray)
    this.setData({
      numberArray: this.data.numberArray
    })
  },
  tapname:function(e){
    console.log(e)
  },
  handleTap1:function(e){
    console.log("handleTap1");
  },
  handleTap2:function(e){
    console.log("handleTap2");
  },
  handleTap3:function(e){
    console.log("handleTap3");
  }
})